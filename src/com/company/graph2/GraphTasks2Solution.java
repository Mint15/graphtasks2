package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.TreeSet;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        HashMap<Integer, Integer> result = new HashMap<>();
        result.put(startIndex, 0);
        deque.offerFirst(startIndex);
        while (!deque.isEmpty()){
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[startIndex][i] != 0){
                    if (result.containsKey(i)){
                        if (result.get(startIndex) + adjacencyMatrix[startIndex][i] < result.get(i)){
                            result.put(i, result.get(startIndex) + adjacencyMatrix[startIndex][i]);
                            if (!deque.contains(i)){
                                deque.offerFirst(i);
                            }
                        }
                    } else {
                        result.put(i, result.get(startIndex) + adjacencyMatrix[startIndex][i]);
                        if (!deque.contains(i)){
                            deque.offerFirst(i);
                        }
                    }
                }
            }
            if (!deque.isEmpty()){
                if(result.get(startIndex) == 0){
                    startIndex = deque.pollLast();
                }
                startIndex = deque.pollLast();
            }
        }
        return result;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int result = 0;
        int startedIndex = 0;
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(startedIndex);
        while (treeSet.size() < adjacencyMatrix.length){
            int v1 = 0;
            int v2 = 0;
            int min = Integer.MAX_VALUE;
            for (Integer i : treeSet){
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (adjacencyMatrix[i][j] > 0 && adjacencyMatrix[i][j] < min && !treeSet.contains(j)){
                        v1 = i;
                        v2 = j;
                        min = adjacencyMatrix[i][j];
                    }
                }
            }

            treeSet.add(v1);
            treeSet.add(v2);
            result += min;
        }

        return result;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
